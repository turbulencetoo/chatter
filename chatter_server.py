#!/usr/bin/env python3.4
'''
chatter_server.py

A simple attempt at a chat program that communitcates
over a unix socket on the network
'''

##############################################################################
#
# Imports
#
##############################################################################
import sys
import socket
import threading
import random
import time

##############################################################################
#
# Globals
#
##############################################################################
SOCKADDR = "es-bb100-2.lab.beer.town"
if "--debug" in sys.argv:
    SOCKADDR = "127.0.0.1"
SOCKPORT = 1569
MAX_COMMUNICATIONS = 16

CONN_DICT = {}  # shared resource needs a lock
CONN_DICT_LOCK = threading.Lock()

LAST_SCRAMBLE = time.time()
NAMES = []
CHATNAME = 'chat'


def load_names():
    global NAMES
    global CHATNAME
    NAMES = []
    with open("names.txt") as fp:
        for line in fp:
            name = line.strip()
            if '=' in name:
                key, val = name.split('=')
                if key == 'chatname':
                    CHATNAME = val
            elif name:
                NAMES.append(name)
    random.shuffle(NAMES)


class Listener(threading.Thread):
    def __init__(self, sock):
        threading.Thread.__init__(self)
        self.sock = sock

    def run(self):
        while True:
            try:
                data = self.sock.recv(256)
                if data:
                    if data[0] == ord('/'):
                        run_special_command(data.decode("utf-8").strip(), self.sock)
                    else:
                        with CONN_DICT_LOCK:
                            broadcast(data, self.sock)
                else:
                    #socket on other end closed
                    with CONN_DICT_LOCK:
                        left_the_chat(self.sock)
                    break
            except (ConnectionResetError, TimeoutError):
                with CONN_DICT_LOCK:
                    left_the_chat(self.sock)
                break


class ClientAcceptor(threading.Thread):
    def __init__(self, sock):
        threading.Thread.__init__(self)
        self.host_sock = sock

    def run(self):
        while True:
            self.host_sock.listen(MAX_COMMUNICATIONS)
            conn, addr = self.host_sock.accept()
            Listener(conn).start()

            with CONN_DICT_LOCK:
                CONN_DICT[conn] = NAMES.pop()
                greet(conn)
                broadcast("has joined the {}".format(CHATNAME).encode("utf-8"), conn)


def broadcast(data, orig_sock):
    # Must wrap calls to broadcast in the CONN_DICT_LOCK
    print("broadcasting", data)
    for conn in CONN_DICT:
        if conn is not orig_sock:
            try:
                name = CONN_DICT.get(orig_sock, "server").encode("utf-8")
                conn.send(name + b"> " + data)
            except IOError:
                print("Write failed")
                left_the_chat(conn)
                break


def run_special_command(cmd, orig_sock):
    cmd_dict = {"/who" : cmd_who,
                "/scramble" : cmd_scramble,
                }
    if cmd in cmd_dict:
        cmd_dict[cmd](orig_sock)


def left_the_chat(sock):
    # Must wrap calls to left_the_chat in the CONN_DICT_LOCK
    name = CONN_DICT[sock]
    NAMES.insert(0, name)
    CONN_DICT.pop(sock)
    server_chat("{} has left the {}".format(name, CHATNAME).encode("utf-8"))

def server_chat(data):
    # Separate broadcast for server messages
    for conn in CONN_DICT:
        try:
            conn.send(b"~" + data)
        except IOError:
            print("Write failed")
            break


def cmd_who(orig_sock):
    orig_sock.send(b"~Online personas:\n")
    with CONN_DICT_LOCK:
        for sock in sorted(CONN_DICT.keys(), key= lambda sock: CONN_DICT[sock]):
            prefix = b""
            name = CONN_DICT[sock].encode("utf-8")
            if sock is orig_sock:
                prefix = b"[you] "
            orig_sock.send(b"~" + prefix + name + b'\n')


def cmd_scramble(orig_sock):
    global LAST_SCRAMBLE
    if time.time() - LAST_SCRAMBLE < (60*30):
        orig_sock.send(b"~Scramble not available")
        return

    with CONN_DICT_LOCK:
        load_names()
        for conn in CONN_DICT:
            CONN_DICT[conn] = NAMES.pop()
        server_chat(b"Names have been scrambled") 
    LAST_SCRAMBLE = time.time()


def greet(sock):
    # Make sure we have CONN_DICT_LOCK upon calling greet 
    name = CONN_DICT[sock]
    greeting = '~Welcome to the {}, {}\n'.format(CHATNAME, name)
    try:
        sock.send(greeting.encode("utf-8"))
    except IOError:
        print("Write failed")
        left_the_chat(conn)
        

def main():
    load_names()
    host_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    host_sock.bind((SOCKADDR, SOCKPORT))
    ClientAcceptor(host_sock).start()

if __name__ == '__main__':
    main()
