#!/usr/bin/env python3
'''
chatter.py

A simple attempt at a chat program that communitcates
over a unix socket on the network
'''

##############################################################################
#
# Imports
#
##############################################################################
import sys
import socket
import curses
import threading
import datetime
import random

##############################################################################
#
# Globals
#
##############################################################################
SOCKADDR = "es-bb100-2.lab.beer.town"
if "--debug" in sys.argv:
    SOCKADDR = "127.0.0.1"
SOCKPORT = 1569

SERVER_METACHAR = '~'

PROMPT = "> "
PROMPT_LEN = len(PROMPT)

LOG_FP = open("chatter.out", "w")

TIMESTAMP_FLAG = False
TIMESTAMP_FORMAT = "%I:%M"
if "--timestamp" in sys.argv:
    TIMESTAMP_FLAG = True
if "--timestamp24" in sys.argv:
    TIMESTAMP_FLAG = True
    TIMESTAMP_FORMAT = "%H:%M"

COLOR_PAIRS = 256
RAND = 0
if "--rand" in sys.argv:
    RAND = random.randrange(0, COLOR_PAIRS)


class Listener(threading.Thread):
    def __init__(self, chatter):
        threading.Thread.__init__(self)
        self.chatter = chatter

    def run(self):
        try:
            self.chatter.listen()
        except KeyboardInterrupt:
            pass  # thread will die here


class Chatter:
    def __init__(self, sock, scr):
        self.stdscr = scr
        self.sock = sock 
        self.cur_line = -1
        self.endl = -1
        self.prompt_next_line()

    def chat(self):
        while True:
            try:
                ch = self.stdscr.getch()
                y, x = self.stdscr.getyx()

                if ch == ord("\n"):
                    curtext = self.stdscr.instr(self.cur_line, PROMPT_LEN).strip()
                    self.sock.send(curtext)
                    self.prompt_next_line()

                elif (ch == curses.KEY_BACKSPACE or ch == 8 or ch == 127):  # ASCII 8 is ^H (backspace), ASCII 127 is DEL
                    if x > PROMPT_LEN:
                        self.backspace()

                elif ch == curses.KEY_LEFT:
                    if x > PROMPT_LEN:
                        self.move_back_one()

                elif ch == curses.KEY_RIGHT:
                    if x < self.endl:
                        self.move_forward_one()

                elif self.endl > 60:  # no lines longer than 60 chars, sorry
                    pass

                elif 32 <= ch <= 126:  # Range of prinatable ascii chars
                    self.stdscr.insch(ch)  # paint the char to the screen
                    self.move_forward_one()
                    self.endl += 1

            except KeyboardInterrupt:
                break
        self.sock.close()  # little hack to close listener thread as well

    def backspace(self):
        self.move_back_one()
        self.stdscr.delch()
        self.endl -= 1

        x = self.stdscr.getyx()[1]
        if x < PROMPT_LEN:
            self.move_forward_one()
        if self.endl < PROMPT_LEN:
            self.endl += 1

    def move_back_one(self):
        y, x = self.stdscr.getyx()
        self.stdscr.move(y, x - 1)

    def move_forward_one(self):
        y, x = self.stdscr.getyx()
        self.stdscr.move(y, x + 1)

    def next_line(self):
        height, width = self.stdscr.getmaxyx()
        if self.cur_line + 1 < height:
            self.cur_line += 1
        else:
            cur_y, cur_x = self.stdscr.getyx()
            self.stdscr.move(0, 0)
            self.stdscr.deleteln()
            self.stdscr.move(cur_y - 1, cur_x)

    def prompt_next_line(self):
        self.next_line()
        self.stdscr.addstr(self.cur_line, 0, PROMPT)
        self.stdscr.move(self.cur_line, PROMPT_LEN)
        self.endl = PROMPT_LEN

    def listen(self):
        while True:
            try:
                data = self.sock.recv(256)
            except socket.error:
                break

            if not data:
                break  # socket on other end closed

            data = data.decode("utf-8").split('\n')
            data = [string for string in data if string]  # removes empty strings

            if not data:
                continue

            # Save cursor position
            prev_x = self.stdscr.getyx()[1]

            # save the current text user has written but not sent
            num_chars_to_save = self.endl - PROMPT_LEN
            curtext = self.stdscr.instr(self.cur_line, PROMPT_LEN, num_chars_to_save)

            # delete that line now that it's saved
            self.stdscr.move(self.cur_line, 0)
            self.stdscr.clrtoeol()

            for line in data:
                p = line.find(PROMPT)
                if line[0] == SERVER_METACHAR:
                    name = "server"
                    msg = line[1:]
                    cpair = (hash(name) + RAND) % COLOR_PAIRS
                    # print full message in server's color
                    self.stdscr.addstr(self.cur_line, 0, msg, curses.color_pair(cpair))
                elif p == -1:
                    self.stdscr.addstr(self.cur_line, 0, line)
                else:
                    name = line[:p]
                    msg = line[p:]
                    cpair = (hash(name) + RAND) % COLOR_PAIRS

                    if TIMESTAMP_FLAG:
                        name = datetime.datetime.now().strftime(TIMESTAMP_FORMAT) + " " + name

                    # print name in color and message in default
                    self.stdscr.addstr(self.cur_line, 0, name, curses.color_pair(cpair))
                    self.stdscr.addstr(self.cur_line, len(name), msg)

                self.next_line()

            new_prompt = PROMPT + curtext.decode("utf-8")
            self.stdscr.addstr(self.cur_line, 0, new_prompt)
            self.stdscr.move(self.cur_line, prev_x)
            self.stdscr.refresh()
            curses.beep()


def init_client():
    client_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_sock.connect((SOCKADDR, SOCKPORT))
    return client_sock


def log(msg, *args):
    print(msg, *args, file=LOG_FP)


def main():
    try:
        # initialize curses
        stdscr = curses.initscr()
        curses.noecho()
        curses.cbreak()
        stdscr.keypad(True)

        # initialize colors
        curses.start_color()
        curses.use_default_colors()
        for i in range(0, COLOR_PAIRS):
            curses.init_pair(i + 1, i, -1)

        client_socket = init_client()

        chatter = Chatter(client_socket, stdscr)
        listener = Listener(chatter)

        listener.start()
        chatter.chat()
    finally:
        curses.nocbreak()
        stdscr.keypad(False)
        curses.echo()
        curses.endwin()
        LOG_FP.close()

if __name__ == '__main__':
    main()
